# Use this function like so:
# add_test(
#   NAME cvt_prb
#   COMMAND ${CMAKE_COMMAND}
#     -DTEST_COMMAND=$<TARGET_FILE:prb>
#     -DVALID_RESULT=${CMAKE_SOURCE_DIR}/testing/data/cvt_prb_output.txt
#     -DTEST_OUTPUT=${CMAKE_BINARY_DIR}/cvt_prb_test_output.txt
#     -P ${PROJECT_SOURCE_DIR}/cmake/file_comparison_test.cmake
# )

# Check arguments.
if (NOT TEST_COMMAND)
   message(FATAL_ERROR "No TEST_COMMAND provided.")
endif()
if (NOT TEST_OUTPUT)
  message(FATAL_ERROR "No TEST_OUTPUT provided (the name of the file the TEST_COMMAND produces).")
endif()
if (NOT VALID_RESULT)
  message(FATAL_ERROR "No VALID_RESULT provided.")
endif()

execute_process(
  COMMAND ${TEST_COMMAND} ${TEST_ARGUMENTS}
  OUTPUT_VARIABLE TMPOUT
)
if (NOT test_not_successful)
  string(LENGTH "${TMPOUT}" NORF)
  message("Wrote ${NORF} bytes to ${TEST_OUTPUT}")
  file(WRITE ${TEST_OUTPUT} ${TMPOUT})
  execute_process(
    COMMAND ${CMAKE_COMMAND} -E compare_files ${VALID_RESULT} ${TEST_OUTPUT}
    RESULT_VARIABLE test_not_successful
    OUTPUT_QUIET
    ERROR_QUIET
  )
endif()
#message("RESULT ${test_not_successful} OUTPUT ${TMPOUT}")

if (test_not_successful)
  message(SEND_ERROR "${TEST_OUTPUT} does not match ${VALID_RESULT}!" )
endif()
